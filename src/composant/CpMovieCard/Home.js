import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
  // Données des films
  const movies = [
    {
      id: 1,
      title: "Film 1",
      description: "Description du film 1",
      trailerLink: "https://lien-de-la-bande-annonce-du-film-1",
    },
    {
      id: 2,
      title: "Film 2",
      description: "Description du film 2",
      trailerLink: "https://lien-de-la-bande-annonce-du-film-2",
    },
    // Ajoutez d'autres films ici
  ];

  return (
    <div>
      <h1>Liste des films</h1>
      <ul>
        {movies.map((movie) => (
          <li key={movie.id}>
            <Link to={`/movie/${movie.id}`}>{movie.title}</Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Home;
