import React from 'react';
import { Link } from 'react-router-dom';


import './MovieCard.css';

const MovieCard = ({ movie  }) => {
  if (!movie) {
    return null; // Or render a placeholder/loading state
  }

  
  return (
    <div className="movie-card">
      <Link to={`/movies/${movie.id}/description`}>
        <img className="poster" src={movie.posterURL} alt={movie.title} />
        <h2 className="title">{movie.title}</h2>
      </Link>
      <p className="rating">Note: {movie.rating}</p>
    </div>
  );
};

export default MovieCard;
