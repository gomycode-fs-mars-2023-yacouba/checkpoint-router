import React from 'react';
import { Link , useParams } from 'react-router-dom';



const TrailerPage = ({ movie }) => {
    const { id } = useParams();

  // Obtenez le film correspondant à l'id de l'URL
  // Vous pouvez utiliser l'id pour rechercher le film dans la liste des films

  return (
    <div>
      <h2>Bande-annonce du film</h2>
      {/* Affichez le lecteur vidéo intégré de la bande-annonce */}
      <p>
        <Link to={`/movies/${id}/description`}>Retour à la description du film</Link>
      </p>
      <p>
        <Link to="/">Retour à la page d'accueil</Link>
      </p>
    </div>
  );
}

export default TrailerPage;
