import React from 'react';
import { Link,  } from 'react-router-dom';



const MovieDescription = ({ match, movies}) => {
    const movieId = match.params.id;
    const movie = movies.find((movie) => movie.id === parseInt(movieId));
  
    if (!movie) {
      return <div>Film non trouvé</div>;
    }
  
    return (
      <div>
        <h2>{movie.title}</h2>
        <p>{movie.description}</p>
        <iframe
          width="560"
          height="315"
          src={movie.trailerLink}
          title="Bande-annonce"
          frameborder="0"
          allowfullscreen
        ></iframe>
        <Link to="/">Retour à la page d'accueil</Link>
      </div>
    );
  }
 
  
  
  

export default MovieDescription;
